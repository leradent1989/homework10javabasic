package tests;

import main.hometask10.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest{
    private FamilyService module;
    private ByteArrayOutputStream output = new ByteArrayOutputStream();
    long millisInDay = 24*3600*1000;
LocalDate dateLoc1 =  LocalDate.of(1973,10,23);
long date1 = dateLoc1.toEpochDay()* millisInDay;
    LocalDate dateLoc2 =  LocalDate.of(1978,9,15);
    long date2 = dateLoc2.toEpochDay()*millisInDay;
    LocalDate dateLoc3 =  LocalDate.of(2010,3,3);
    long date3 = dateLoc3.toEpochDay()*millisInDay;
    LocalDate dateLoc4 =  LocalDate.of(2018,8,8);
    long date4 = dateLoc4.toEpochDay()*millisInDay;
    List<Family> families = new ArrayList<>(List.of(
            new Family(new Human("gerge1", "grgr1",date1 ), new Human("gdgd1", "dgdgd1", date2)),
            new Family(new Human("gerge2", "grgr2", date1), new Human("gdgd2", "dgdgd2", date2)),
            new Family(new Human("gerge3", "grgr3", date1), new Human("gdgd3", "dgdgd3", date2)),
            new Family(new Human("gerge4", "grgr4", date1), new Human("gdgd4", "dgdgd4", date2)),
            new Family(new Human("gerge5", "grgr5", date1), new Human("gdgd5", "dgdgd5", date2)),
            new Family(new Human("gerge6", "grgr6", date1), new Human("gdgd6", "dgdgd6", date2))
    ));

    @BeforeEach
    public void setUp() {

        CollectionFamilyDao familyDao = new CollectionFamilyDao(families);
        module = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(module);


    }

    @Test
    void getAllFamilies() {
        List<Family> actual = module.getAllFamilies();
        List<Family> expected = families;
        assertEquals(expected, actual);
    }

    @Test
    void displayAllFamilies() {
        PrintStream old = System.out;

        System.setOut(new PrintStream(output));
        module.displayAllFamilies();
        assertEquals(output.toString().replaceAll("\n", ""), "Human{name = gdgd1 surname = dgdgd1 birthDate = 15/09/1978 iq = 0 schedule = null}"+
                "Human{name = gerge1 surname = grgr1 birthDate = 23/10/1973 iq = 0 schedule = null}"+
                "Human{name = gdgd2 surname = dgdgd2 birthDate = 15/09/1978 iq = 0 schedule = null}"+
                "Human{name = gerge2 surname = grgr2 birthDate = 23/10/1973 iq = 0 schedule = null}"+
                "Human{name = gdgd3 surname = dgdgd3 birthDate = 15/09/1978 iq = 0 schedule = null}"+
                "Human{name = gerge3 surname = grgr3 birthDate = 23/10/1973 iq = 0 schedule = null}"+
                "Human{name = gdgd4 surname = dgdgd4 birthDate = 15/09/1978 iq = 0 schedule = null}"+
                "Human{name = gerge4 surname = grgr4 birthDate = 23/10/1973 iq = 0 schedule = null}"+
                "Human{name = gdgd5 surname = dgdgd5 birthDate = 15/09/1978 iq = 0 schedule = null}"+
                "Human{name = gerge5 surname = grgr5 birthDate = 23/10/1973 iq = 0 schedule = null}"+
                "Human{name = gdgd6 surname = dgdgd6 birthDate = 15/09/1978 iq = 0 schedule = null}"+
                "Human{name = gerge6 surname = grgr6 birthDate = 23/10/1973 iq = 0 schedule = null}", "Successfully brings text");

        System.setOut(old);
    }


    @Test
    void getFamilyById() {
        Family actual = module.getFamilyById(1);
        Family expected = families.get(1);
        assertEquals(expected, actual);
    }

    @Test
    void count() {
        int actual = module.count();
        int expected = families.size();
        assertEquals(expected, actual);
    }

    @Test
    void createNewFamily() {
        int size = families.size();
        module.createNewFamily(new Human("mother", "dgrg7", date1), new Human("father", "grrr7", date2));
        int actual = families.size();
        int expected = size + 1;
        assertEquals(expected, actual);
    }

    @Test
    void deleteFamilyByIndex() {
        Family actual = families.get(2);
        module.deleteFamilyByIndex(1);
        Family expected = families.get(1);
        assertEquals(expected, actual);
    }

    @Test
    void getPets() {
        Set<Pet> actual = module.getPets(2);
        Set<Pet> expected = families.get(2).getPet();
        assertEquals(expected, actual);
    }

    @Test
    void addPet() {
        Pet pet = new Dog("snoopy", 3, (byte) 65);
        module.addPet(2, pet);
        Set<Pet> actual = new HashSet<Pet>(Set.of(pet));
        Set<Pet> expected = families.get(2).getPet();
        assertEquals(expected, actual);
    }

    @Test
    void getFamiliesBiggerThen() {
        List<Family> actual = module.getFamiliesBiggerThen(1);
        List<Family> expected = new ArrayList<>();
        List<Family> newFamilies = new ArrayList<>();
        families.forEach(family -> {
            if (family.countFamily(family.getChildren()) > 1) {
                newFamilies.add(family);
            }
        });
        expected = newFamilies;
        assertEquals(expected, actual);
    }

    @Test
    void countFamiliesWithMemberNumber() {
        int actual = module.countFamiliesWithMemberNumber(2);
        int expected = 6;
        assertEquals(expected, actual);
    }

    @Test
    void getFamiliesLessThen() {
        List<Family> actual = module.getFamiliesLessThen(3);
        List<Family> expected = new ArrayList<>();
        List<Family> newFamilies2 = new ArrayList<>();
        families.forEach(family -> {
            if (family.countFamily(family.getChildren()) < 3) {
                newFamilies2.add(family);
            }
        });
        expected = newFamilies2;
        assertEquals(expected, actual);
    }

    @Test
    void adoptChild() {
        Human actual = module.adoptChild(families.get(1), new Human("child", "surname", date4))
                .getChildren().get(0);
        Human expected = families.get(1).getChildren().get(0);
        assertEquals(expected, actual);
    }

    @Test
    void bornChild() {
        module.bornChild(families.get(1), "Jane", "Sam");
        String name = families.get(1).getChildren().get(0).getName();
        boolean actual;
        if (name.equals("Jane") || name.equals("Sam")) {
            actual = true;
        } else actual = false;
        boolean expected = true;
        assertEquals(expected, actual);
    }

    @Test
    void deleteAllChildrenOlderThen() {
        for (int i = 0; i < families.size(); i++) {
            families.get(i).getChildren().add(new Human("fggd", "dgddg", date4));
            families.get(i).getChildren().add(new Human("fggd", "dgddg", date3));
        }
        boolean actual;
        int age = 10;
        module.deleteAllChildrenOlderThen(age);
        List<Human> olderChildren = new ArrayList<>();

        for (int i = 0; i < families.size(); i++) {

            for (int k = 0; k < families.get(i).getChildren().size(); k++) {

                LocalDate birthDay = Instant.ofEpochMilli(families.get(i).getChildren().get(k).getBirthdate())
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();


                LocalDate now = LocalDate.now();
                Period period = Period.between(birthDay, now);

                if (period.getYears() > age) {
                    olderChildren.add(families.get(i).getChildren().get(k));
                } else continue;
            }

            if (olderChildren.size() > 0) {
                actual = false;
            } else actual = true;
            boolean expected = true;
            assertEquals(expected, actual);
        }


    }

}





