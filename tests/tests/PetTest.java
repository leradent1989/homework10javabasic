package tests;

import main.hometask10.Dog;
import main.hometask10.DomesticCat;
import main.hometask10.Pet;
import main.hometask10.RoboCat;
import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PetTest {
    private Pet module;

    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Test
    public void  petToString(){
        module = new DomesticCat("Mura",5,(byte) 60);
        String actual = module.toString();
        String expected = "DOMESTICCAT{nickname=Mura, age=5, trickLevel=60 , habits=null}";
        assertEquals(expected,actual);
    }
    @Test

    public void testPetEat(){
        PrintStream old=System.out;
        Pet pet=new Dog("snoopy",3,(byte) 50);
        System.setOut(new PrintStream(output));
        pet.eat();
        assertEquals(output.toString().replaceAll("\n",""),"Я кушаю","Successfully brings text");

        System.setOut(old);
    }

    @Test

    public void testDogFoul(){
        PrintStream old=System.out;
        Dog pet=new Dog("snoopy",3,(byte) 70);
        System.setOut(new PrintStream(output));
        pet.foul();
        assertEquals(output.toString().replaceAll("\n",""),"Нужно замести следы ...","Successfully brings text");

        System.setOut(old);
    }
    @Test

    public void testCatFoul(){
        PrintStream old=System.out;
        DomesticCat pet=new DomesticCat("Mura",3,(byte) 70);
        System.setOut(new PrintStream(output));
        pet.foul();
        assertEquals(output.toString().replaceAll("\n",""),"Поточу ка я когти об этот диван ...","Successfully brings text");

        System.setOut(old);
    }
    @Test

    public void testRoboCatFoul(){
        PrintStream old=System.out;
        RoboCat pet=new RoboCat("K5",3,(byte) 70);
        System.setOut(new PrintStream(output));
        pet.foul();
        assertEquals(output.toString().replaceAll("\n",""),"Роботы не делают глупостей ...","Successfully brings text");

        System.setOut(old);
    }
    @Test
    public void testCatRespond(){
        PrintStream old=System.out;
        DomesticCat pet=new DomesticCat("Mura",5,(byte) 60);
        System.setOut(new PrintStream(output));
        pet.respond();
        assertEquals(output.toString().replaceAll("\n",""),"Привет хозяин Я DOMESTICCAT Mura cоскучилcя","Successfully brings text");

        System.setOut(old);
    }
    @Test
    public void testDogRespond(){
        PrintStream old=System.out;
        Dog pet=new Dog("Max",5,(byte) 60);
        System.setOut(new PrintStream(output));
        pet.respond();
        assertEquals(output.toString().replaceAll("\n",""),"Привет хозяин Я DOG Max cоскучилcя","Successfully brings text");

        System.setOut(old);
    }
}

