package main.hometask10;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }


    // Методы после рефакторинга с использованием нового Java8(stream,lambda)------------------------------------------------------------------

    //1
    public void displayAllFamilies() {
        List<Family> families = familyDao.findAll();

        families.stream().forEach(family -> family.toString());
    }


    //2
    public List<Family> getFamiliesBiggerThen(int count) {
        List<Family> families = familyDao.findAll();

        families = families.stream()
                .filter(family ->family.countFamily(family.getChildren()) > count)
                .collect(Collectors.toList());

        return families;
    }


    //3
    public int   countFamiliesWithMemberNumber (int count) {
        List<Family> families = familyDao.findAll();

        families = families.stream()
                .filter(family ->family.countFamily(family.getChildren()) == count )
                .collect(Collectors.toList()); ;

        return families.size();

    }

    //4
    public List<Family> getFamiliesLessThen(int count) {
        List<Family> families = familyDao.findAll();
        families = families.stream()
                .filter(family ->family.countFamily(family.getChildren()) < count )
                .collect(Collectors.toList()); ;

        return families;
    }


    // 5
    public void deleteAllChildrenOlderThen(int age){
        List<Family> families = familyDao.findAll();

        families.stream().forEach(family -> {
            List<Human> familyChildren = family.getChildren();
            Iterator<Human> itr = familyChildren.iterator();

            for (; itr.hasNext(); ) {
                Period period = Period.between(Instant.ofEpochMilli(itr.next().getBirthdate())
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate(),LocalDate.now());
                if (period.getYears() > age) {
                    itr.remove();
                }

            }
        });
    }

    //Другие методы ---------------------------------------------------------------------------------------------------
    //1
    public List <Family> getAllFamilies() {

        List<Family> families1 = familyDao.findAll();
        return families1;
    }
    //2
    public Family getFamilyById(int index) {

        Family family = familyDao.getFamilyByIndex(index);

        return family;
    }

    //3
    public int count() {
        List<Family> families = familyDao.findAll();
        int listLength = families.size();
        return listLength;
    }

    //4
    public void createNewFamily(Human mother, Human father) {
        Family newFamily = new Family(mother, father);
        familyDao.save(newFamily);
    }

    //5
    public boolean deleteFamilyByIndex(int index) {
        int length = familyDao.findAll().size();
        boolean result = familyDao.deleteFamily(index);

        if(familyDao.findAll().size() < length){
            return true;
        }else return false;}




    //6
    public Set<Pet> getPets(int index) {
        Family family = familyDao.getFamilyByIndex(index);
        Set<Pet> pets = family.getPet();
        return pets;
    }

    //7
    public void addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        Set<Pet> pets = family.getPet();
        pets.add(pet);
    }




    //8
    public Family  adoptChild(Family family,Human child){
        List<Family> families = familyDao.findAll();
       int index = families.indexOf(family);
       Family currentFamily = families.get(index);
       currentFamily.addChild(child);

       return currentFamily;
    }
//9
    public Family bornChild(Family family,String girlName,String boyName){
        List<Family> families = familyDao.findAll();
        int index = families.indexOf(family);
        Random random =new Random();
        boolean isBoy = random.nextBoolean();
        String  name = (isBoy? boyName : girlName);
        Family currentFamily = families.get(index);
        Calendar calendar = new GregorianCalendar();
        long birthDay = calendar.getTimeInMillis();

        currentFamily.addChild(new Human(name,currentFamily.getFather().getSurname(),birthDay ));

        return currentFamily;
    }

    }


