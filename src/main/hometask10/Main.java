package main.hometask10;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        long millisInDay = 24*3600*1000;
        LocalDate dateLoc1 =  LocalDate.of(1973,10,23);
        long date1 = dateLoc1.toEpochDay()*millisInDay;
        LocalDate dateLoc2 =  LocalDate.of(1978,9,15);
        long date2 = dateLoc2.toEpochDay()*millisInDay;
        LocalDate dateLoc3 =  LocalDate.of(2010,3,3);
        long date3 = dateLoc3.toEpochDay()*millisInDay;
        LocalDate dateLoc4 =  LocalDate.of(2018,8,8);
        long date4 = dateLoc4.toEpochDay()*millisInDay;
        List<Family> families = new ArrayList<>(List.of(
                new Family(new Human("gerge1","grgr1",date1),new Human("gdgd1","dgdgd1",date2)),
                new Family(new Human("gerge2","grgr2",date1),new Human("gdgd2","dgdgd2",date2)),
                new Family(new Human("gerge3","grgr3",date1),new Human("gdgd3","dgdgd3",date2)),
                new Family(new Human("gerge4","grgr4",date1),new Human("gdgd4","dgdgd4",date2)),
                new Family(new Human("gerge5","grgr5",date1),new Human("gdgd5","dgdgd5",date2)),
                new Family(new Human("gerge6","grgr6",date1),new Human("gdgd6","dgdgd6",date2))
        ));
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);



     // 2 displayAllFamilies
        System.out.println("displayAllFamilies: ");

        familyController.displayAllFamilies();

        //5 adoptChild
        System.out.println("adoptChild: ");

        familyController.adoptChild( families.get(2),new Human("michael","AdoptedChild",date3));

        familyController.getFamilyById(2).toString();

        System.out.println("---------------------------------------------");

        //6 bornChild
        System.out.println("bornChild: ");

        System.out.println("Family before applying born child: ");
        families.get(0).toString();
        familyController.bornChild(families.get(0),"Peter","Mary");
        System.out.println("Family after applying born child");
        families.get(0).toString();
        System.out.println("---------------------------------------------------------");

        // 7 getFamiliesLessThen
        System.out.println("getFamiliesLessThen: ");

        familyController.getFamiliesLessThen(3).forEach(family -> family.toString());

        System.out.println("------------------------------------------------");

        // 8 getFamiliesBiggerThen
        System.out.println("getFamiliesBiggerThen: ");

        familyController.getFamiliesBiggerThen(2).forEach(family -> family.toString());

        System.out.println("9 ----------------------------------------------------");



        // 9 countFamilyWithMemberNumber
        System.out.println("countFamiliesWithMemberNumber: ");

        System.out.println("Number of families:" + families.size());
        System.out.println("------------------------------");
        System.out.println("Number of families with 3 members: " + familyController.countFamiliesWithMemberNumber(3));

        System.out.println("  ----------------------------------------------------");
        //14 DeleteAllChildrenOlderThen
        System.out.println("DeleteAllChildrenOlderThen : first apply method  - next try to find children with age more then this age in families and if present add them to new list: ");

        for (int i = 0; i < families.size(); i++) {
            families.get(i).getChildren().add(new Human("child1", "child1", date4));
            families.get(i).getChildren().add(new Human("child2", "child2",date3 ));
        }

        int age = 10;

        familyController.deleteAllChildrenOlderThen(age);

        List<Human> olderChildren = new ArrayList<>();


        for (int i = 0; i < families.size(); i++) {

            for (int k = 0; k < families.get(i).getChildren().size(); k++) {

                LocalDate birthDay = Instant.ofEpochMilli(families.get(i).getChildren().get(k).getBirthdate())
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();


                LocalDate now = LocalDate.now();
                Period period = Period.between(birthDay,now);

                if (period.getYears() > age) {
                    olderChildren.add(families.get(i).getChildren().get(k));
                }
            }
        }
        System.out.println("Children older then 10 years in list families after applying method ");
        System.out.println(olderChildren +" - there is no children more then 10 years,method works right");
        families.forEach(family -> {
            System.out.println("-------------------------------------");
            family.toString();
        });
        // 1 getAllFamilies
        System.out.println("getAllFamilies: ");

        familyController.getAllFamilies().forEach(family -> family.toString());

       //3  Count
        System.out.println("count all families: ");

        System.out.println(familyController.count());

       //4 getFamilyById
        System.out.println("getFamilyById: ");

       familyController.getFamilyById(2).toString();

       System.out.println("------------------------------------------------");



     // 10 CreateNewFamily
        System.out.println("createNewFamily: ");

        System.out.println("Last family in list before applying method :");
       families.get(families.size() -1).toString();
       System.out.println("Last family in list after applying method :");
        familyController.createNewFamily(new Human("vegg7","drr7",date1),new Human("gerg7","dsgt7",date2));

    families.get(families.size() -1).toString();
        // 11 Add pet

        System.out.println("addPet: ");

        familyController.addPet(0,new DomesticCat("Mura",5,(byte) 56));
        familyController.addPet(0,new DomesticCat("Pusha",7,(byte) 36));

        familyController.getFamilyById(0).toString();
    // 12 getPets
        System.out.println("getPets: ");

    System.out.println("Pets of the first family in list:");
    Arrays.toString( familyController.getPets(0).toArray());

    // 13 Delete family by index :
        System.out.println("deleteFamilyByIndex: ");

        System.out.println(" Family in list with index 2  before applying method: ");
        families.get(2).toString();
        familyController.deleteFamilyByIndex(2);
        System.out.println(" Family in list with index 2  after applying method: ");
        families.get(2).toString();


}

}
