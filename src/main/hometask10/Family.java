package main.hometask10;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

 public class Family {

    private  Human mother;
    private  Human father;
    private  List <Human>  children;
    private   Set <Pet> pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
        children = new ArrayList<Human>(0);
        pet = new HashSet<>();

    }

    public Human getMother(){

        return  mother;
    }

    public  Human getFather(){
        return  father;
    }

    public List<Human> getChildren(){
        return  children;
    }

    public Set <Pet> getPet (){

        return pet;
    }

    public void setPet(Set <Pet> familyPet){

        pet = familyPet;
    }


    public  void addChild(Human child) {


        children.add(child);

        child.setFamily(this);
    }

    public int countFamily(List<Human> children){

        return children.size() + 2;
    }

    public boolean deleteChild( int index){
     if(index < 0 || children == null || index > (children.size()-1)){
            return false;
        }
        int length = children.size();
        children.remove(index);
        if(length > children.size()){
            return true;
        } else return false;
    }
    public boolean deleteChild( Human child){


        int length = children.size();
        children.remove(child);
        if(length > children.size()){
            return true;
        } else return false;

    }
    @Override
    public  String toString(){
        String str = "";
        for(int i = 0;i < children.size();i++){
            str = str + children.get(i).toString() ;
        }
        String message = str + this.father.toString() + this.mother.toString() + (this.pet == null? "null" : this.pet.toString() ) ;

        return message;
    }
    @Override
    public int hashCode(){
        int result = this.getMother() == null?0:this.getMother().hashCode();
        result = this.getFather() == null? result : result + this.getFather().hashCode();

        return result;
    }
    @Override
    public boolean equals(Object obj){

        if(obj == null){
            return  false;
        }
        if(!(obj.getClass() == Family.class)){
            return false;
        }
        Family family = (Family) obj;
        Human familyMother = family.getMother();
        Human familyFather = family.getFather();
        if((familyMother == this.mother || familyMother.equals(this.mother))  &&
                (familyFather == this.father || familyFather.equals(this.father))) {
            return true;
        }else  return false;

    }

    public static void main(String[] args) {
        Human Peter =new Human("Peter","Tomson",1960);
        Human Helen = new Human("Helen","Tomson",1965);
        Family Tomson = new Family(Peter,Helen);
        Pet Pusha = new DomesticCat("Pusha",5,(byte) 50);
        HashSet <String> habits =  new HashSet <String> (Set.of("eat","drink","sleep"));
        Pusha.setHabits(habits);
        Set pet =new HashSet<>(Set.of(Pusha));
        Tomson.setPet(pet);
        Human Michael = new Human("Michael","Tomson",1994);
        Tomson.addChild(Michael);
        Tomson.toString();
        Tomson.deleteChild(0);
        System.out.println("-------------------");
        Tomson.toString();
        Michael.describePet(Pusha);

    }

}
