package main.hometask10;

import java.util.List;
import java.util.Set;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;

    }
    public List<Family> getAllFamilies(){
        return familyService.getAllFamilies();
    }
    public void displayAllFamilies(){
        familyService.displayAllFamilies();
    }
    public Family getFamilyById(int index){
        return familyService.getFamilyById(index);
    }
    public int count(){
        return familyService.count();
    }
    public void createNewFamily(Human mother, Human father){
        familyService.createNewFamily(mother,father);
    }
    public boolean deleteFamilyByIndex(int index){
        return familyService.deleteFamilyByIndex(index);
    }
    public Set<Pet> getPets(int index){
        return  familyService.getPets(index);
    }
    public void addPet(int index, Pet pet){
        familyService.addPet(index,pet);
    }
    public List<Family> getFamiliesBiggerThen(int size){
        return  familyService.getFamiliesBiggerThen(size);
    }
    public int   countFamiliesWithMemberNumber (int size){
        return familyService.countFamiliesWithMemberNumber(size);
    }
    public List<Family> getFamiliesLessThen(int size){
        return familyService.getFamiliesLessThen(size);
    }
    public Family  adoptChild(Family family,Human child){
        return familyService.adoptChild(family,child);
    }
    public void deleteAllChildrenOlderThen(int age){
        familyService.deleteAllChildrenOlderThen(age);
    }
    public Family bornChild(Family family,String girlName,String boyName){
      return   familyService.bornChild(family,girlName,boyName);
    }

}
