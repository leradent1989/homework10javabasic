package main.hometask10;

import java.util.HashSet;
import java.util.Set;

public class Dog extends Pet implements PetInterface{




    public Dog(String nickName,int age,byte trickLevel) {
        super( nickName, age, trickLevel);
        species = Species.DOG;
    }

    public void respond() {
        System.out.println("Привет хозяин Я " + species+ " "+ this.getNickName() + " cоскучилcя");
    }

    public void foul(){
        System.out.println("Нужно замести следы ...");
    }


    public static void main(String[] args) {
        Dog  snoopy = new Dog("Mimi",5,(byte) 30);
        HashSet <String> habits = new HashSet<>(Set.of("sleep","drink","eat"));
        snoopy.setHabits(habits);
        snoopy.setNickName("snoopy");
        snoopy.toString();
        snoopy.respond();
       System.out.println( snoopy.getSpecies());
    }

}
