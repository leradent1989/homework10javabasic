package main.hometask10;

import java.util.HashSet;
import java.util.Set;

 public class  Fish extends Pet {



  public Fish(String nickName,int age,byte trickLevel){
   super(nickName,age,trickLevel);
   this.species = Species.FISH;

  }

  public  void respond (){
   System.out.println("Привет хозяин Я " + this.getNickName() +"cоскучилась");
  };


  public static void main(String[] args) {
   Fish fish = new Fish("goldfish",3,(byte) 10);
   HashSet<String> habits = new HashSet<>(Set.of("sleep","drink","eat"));
   fish.setHabits(habits);
   fish.toString();
   System.out.println(fish.getSpecies());
  }

 }
