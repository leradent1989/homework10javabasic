package main.hometask10;

import java.util.HashSet;
import java.util.Set;


 interface PetInterface{
     void eat();
     void respond();
     void foul();

}



    public abstract class Pet  {

    static   Species species = Species.UNKNOWN;
    private  String nickName;
    private  int age;
    private  byte trickLevel;
    private Set <String> habits;

    public Pet(){}

    public Pet(String nickname){

        this.nickName = nickname;

    }

    public Pet(String nickName,int age,byte trickLevel){

        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;

    }

    public Species getSpecies(){
        return species;
}

    public  String  getNickName(){
        return  nickName;

    }
    public void setNickName(String petNickname){

        nickName = petNickname;
    }
    public  int  getAge(){

        return  age;

    }

    public void setAge(int petAge){

        age = petAge;
    }
    public  byte  getTrickLevel(){

        return  trickLevel;

    }

    public void setTrickLevel(byte petTrickLevel){

        trickLevel = petTrickLevel;
    }

    public  HashSet<String> getHabits(){

        return (HashSet<String>) habits;

    }

    public void setHabits(HashSet<String> petHabits){

        habits = petHabits;
    }


    public  void eat(){
        System.out.println("Я кушаю");
    }


    public  abstract void respond ();



     @Override
     public   String  toString(){

         String message =  species + "{nickname=" + nickName +", age=" + age +", trickLevel=" + trickLevel +" , habits=" + habits+ "}" ;
         System.out.println(message);
         return message;
     }
     @Override
     public int hashCode(){
         int result = this.getNickName() == null?0:this.getNickName().hashCode();
         result = result +age;
         result = result + (int) trickLevel;
         return result;
     }
     @Override
     public boolean equals(Object obj){

         if(obj == null){
             return  false;
         }
         if(!(obj.getClass() == Pet.class)){
             return false;
         }
         Pet pet = (Pet) obj;
         int petAge = pet.getAge();
         String petNickName = pet.getNickName();
         Species petSpecies =pet.getSpecies();
         if(petAge == this.age  &&
                 (petNickName == this.nickName || petNickName.equals(this.nickName))  &&
                 (petSpecies == species || petSpecies.equals(species))) {
             return true;
         }else  return false;

     }



 }

enum  Species {
     UNKNOWN,
    DOMESTICCAT,
    ROBOCAT,
    DOG,
    BIRD,
    FISH,
    MOUSE
}