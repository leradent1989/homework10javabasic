package main.hometask10;

import java.time.DayOfWeek;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public final class Woman extends Human {

    public Woman(){

        super();
    }

    public Woman(String name,String surname,int year,Family family){

        super(name,surname,year,family);
    }

    public Woman(String name, String surname, int year, Family family, HashMap<DayOfWeek,String> schedule){

        super(name,surname,year,family,schedule);
    }
    @Override
    public void greetPet(Pet pet){
        System.out.println("Привет " + pet.getNickName() + " сейчас я тебя накормлю");
    }

    public boolean doMakeUp(String makeupKit){
        boolean bool = false;
        String makeup = new String("makeup");
        System.out.println("Нужно привести себя впорядок");
        makeup = makeup + makeupKit;
        if(makeup.equals("makeupDone")){
            bool = true;
        }

        return bool;
    }

    public static void main(String[] args) {
        Woman Mary = new Woman();
        System.out.println(Mary.doMakeUp("Done"));
        Mary.toString();
        Woman mother = new Woman();
        Man father = new Man();
        Family smith = new Family(mother,father);
        Mary.setFamily(smith);
        Pet Mimi = new Dog("Mimi",5,(byte) 60);
        HashSet <String> habits = new HashSet<>(Set.of("eat","drink","sleep"));
        Mimi.setHabits(habits);
        Mimi.toString();
        Mary.greetPet(Mimi);

    }
}
