package main.hometask10;

import java.time.DayOfWeek;
import java.util.HashMap;

public final class Man extends Human {

    public Man(){
        super();
    }

    public Man(String name,String surname,int year,Family family){
        super(name,surname,year,family);
    }
    public Man(String name, String surname, int year, Family family, HashMap<DayOfWeek,String> schedule){
        super(name,surname,year,family,schedule);
    }
    @Override
    public void greetPet(Pet pet){
      System.out.println("Привет " + pet.getNickName() + " твой хозяин вернулся");
    }

    public boolean fixCar(String instruments){
        boolean bool = false;
        String car = new String("car");
        System.out.println("Пойду возьму инструменты");
        car = car + instruments;
        if(car.equals("carFixed")){
            bool = true;
        }

        return bool;
    }

    public static void main(String[] args) {
        Man George = new Man();
        System.out.println(George.fixCar("Fixed"));

    }
}
