package main.hometask10;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;



public class Human {

    private    String name;
    private    String surname;
    protected long  birthDate;
    private    byte iq;
    private    Family family;
    private   Map <DayOfWeek,String> schedule;


    public  Human(){

    }
    public  Human( String name, String surname,byte iq){
        this.name =name;
        this.surname = surname;
        this.iq = iq;


    }
    public  Human( String name, String surname,String birthDay,byte iq){
        this.name =name;
        this.surname = surname;
        this.iq = iq;
        try{
            Date date=new SimpleDateFormat("dd/MM/yyyy").parse(birthDay);
            this.birthDate = date.getTime();

        }catch(Exception e){
            System.out.println(e);

        }

    }
    public  Human( String name, String surname,long birthDate){
        this.name =name;
        this.surname = surname;
        this.birthDate =  birthDate;
    }

    public  Human(String name,String surname, long birthDate,Family family){
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.family = family;
    }

    public  Human(String name,String surname,long birthDate,Family family,HashMap <DayOfWeek,String> shedule){
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.family = family;
        this.schedule = shedule;
    }
    public   String  getName(){
        return  name;

    }

    public void setName(String humanName){

        name = humanName;
    }

    public  String  getSurname(){
        return  surname;

    }
    public void setSurname(String humanSurname){

        surname = humanSurname;
    }

    public  long   getBirthdate(){

        return   birthDate ;
    }

    public void setBirthDate(long  birthDay){

        birthDate  = birthDay ;
    }
    public  byte getIq(){
        return  iq;
    }

    public Family getFamily(){
        return family;
    }

    public  void setFamily(Family humanFamily){
        family = humanFamily;

    }
    public HashMap<DayOfWeek, String> getSchedule(){

        return (HashMap<DayOfWeek, String>) schedule;
    }
    public  void setSchedule (HashMap<DayOfWeek,String> humanSchedule){
        schedule = humanSchedule;
    }

    public  void greetPet ( Pet pet){

        System.out.println("Привет " + pet.getNickName() );
    }
    public   void  describePet(Pet pet){

        System.out.println( "У меня есть " + pet.getSpecies() +", ему "+ pet.getAge() + " лет, он "+ (pet.getTrickLevel() > 50? "очень хитрый":"почти не хитрый"));
    }
    public void describeAge(){
        LocalDate birthDay = Instant.ofEpochMilli(birthDate)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        System.out.println(birthDay.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        LocalDate now = LocalDate.now();
        Period period = Period.between(birthDay,now);
        System.out.println("Мне " + period.getYears() + " лет " + period.getMonths() + " месяцев " + period.getDays() + " дней");
    }

    @Override
    public  String toString (){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String formatted = format.format(birthDate);

        String message = "Human{name = " + name + " surname = " + surname +" birthDate = "+
                formatted + " iq = " + iq + " schedule = " + schedule +
                "}" ;
        System.out.println(message);
        return message;
    }
    @Override
    public int hashCode(){
        int  result = this.getSurname() == null?0:this.getSurname().hashCode();
        result = result +  iq;
        return result;
    }
    @Override
    public boolean equals(Object obj){

        if(obj == null){
            return  false;
        }
        if(!(obj.getClass() == Human.class)){
            return false;
        }
        Human human = (Human) obj;
        String humanName = human.getName();
        String humanSurname = human.getSurname();
        Family humanFamily = human.getFamily();
        if((humanName == this.name  || humanName.equals(this.name)) &&
                ( humanSurname == this.surname ||  humanSurname.equals(this.surname))  &&
                (humanFamily == this.family || humanFamily.equals(this.family))) {
            return true;
        }else  return false;

    }

    public static void main(String[] args) {

        GregorianCalendar birthDay = new GregorianCalendar(1977,0,23);
        System.out.println(birthDay.getTimeInMillis());
        Human Sam =new Human("Sam","Smith", birthDay.getTimeInMillis());

        Sam.toString();
        Sam.describeAge();
        String johnsBirthday = "18/07/1999";


        Human John = new AdoptedChild("John","Smith",johnsBirthday,(byte) 112);

        John.toString();

        Human Jake =new Human("Jake","Tomson","12/03/2000",(byte) 115);
        Jake.toString();

    }


}
